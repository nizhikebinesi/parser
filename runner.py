from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess

from otparser import settings
from otparser.spiders.online_trade import OnlineTradeSpider
from urllib import request

if __name__ == "__main__":
    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    search = request.quote_plus("материнская плата asus".encode("cp1251"))
    print(search)
    process = CrawlerProcess(settings=crawler_settings)
    process.crawl(OnlineTradeSpider, search=search)

    process.start()
