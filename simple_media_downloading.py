import requests


def get_extension(headers):
    if "Content-Type" not in headers:
        raise ValueError(f"There are no Content-Type in headers: {headers}")
    return headers["Content-Type"].split("/")[-1]

url = "https://www.avito.ru/rossiya?q=котята"
# url = "https://www.chopochom.com/front/wp-content/uploads/2017/01/zen.jpg"
r = requests.get(url)
print(r.headers["Content-Type"])
print()


