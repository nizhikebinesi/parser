import scrapy
from scrapy.http import HtmlResponse


class OnlineTradeSpider(scrapy.Spider):
    name = 'online_trade'
    allowed_domains = ['avito.ru']
    # start_urls = ['https://www.avito.ru/rossiya?q=rtx2080']

    def __init__(self, search):
        super().__init__()
        self.start_urls = [f"https://www.onlinetrade.ru/sitesearch.html?query={search}"]

    def parse(self, response: HtmlResponse):
        print(response)
        links = response.xpath("//a[contains(@class,'__item__name')]")
        print(links)
        pass
